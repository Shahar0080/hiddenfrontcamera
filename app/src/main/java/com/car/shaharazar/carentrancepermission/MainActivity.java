package com.car.shaharazar.carentrancepermission;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    public static final String version = "1.0";

    //An array of the permissions we need
    String[] PERMISSIONS = {
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    //The folder we save the images in
    String dirPath = Environment.getExternalStorageDirectory().toString() + "/IWantCarEntrancePermission";

    Button bBeep;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        askForPermissions();
        defineButton();
    }


    /**
     * This function defines and sets listener to the button
     */
    private void defineButton(){
        Log.i(TAG, "Defining button at @defineButton");
        bBeep = findViewById(R.id.button);

        bBeep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!hasPermissions(MainActivity.this, PERMISSIONS)){
                    Toast.makeText(MainActivity.this, "Please accept permissions.", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS, 1);
                    return;
                }
                Toast.makeText(MainActivity.this, "Taking picture..", Toast.LENGTH_SHORT).show();
                takePicture();


            }
        });
    }

    /**
     * This function does the picture taking
     */
    private void takePicture(){
        Log.i(TAG, "Take picture operation started @takePicture");
        File directory = new File(dirPath);

        if (!directory.exists()) {
            directory.mkdir();
        }

        Camera camera = Camera.open(1);
        Camera.Parameters parameters = camera.getParameters();
        parameters.setPictureFormat(ImageFormat.JPEG);
        camera.setParameters(parameters);
        SurfaceView mview = new SurfaceView(getBaseContext());
        try {
            camera.setPreviewDisplay(mview.getHolder());
            camera.setPreviewDisplay(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        SurfaceTexture surfaceTexture = new SurfaceTexture(10);
        try {
            camera.setPreviewTexture(surfaceTexture);
        } catch (IOException e) {
            e.printStackTrace();
        }
        camera.startPreview();
        camera.takePicture(null,null,photoCallback);
    }

    /**
     * The camera callback. Saves the image.
     */
    Camera.PictureCallback photoCallback=new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            Log.i(TAG, "Camera callback called");
            try {
                Random rand = new Random();
                int  n = rand.nextInt(50) + 1;
                File myDir = new File(dirPath);
                File file = new File (myDir, "CarPermission" + n + ".jpeg");
                FileOutputStream out = new FileOutputStream(file);
                out.write(data);
                out.flush();
                out.close();
                scanner(file.getAbsolutePath());
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Success")
                        .setMessage("Picture saved! \n Path: " + file.getAbsolutePath())
                        .setCancelable(true).show();
            } catch (FileNotFoundException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();

            } catch (Exception e)
            {
                e.printStackTrace();
            }
            camera.stopPreview();

        }
    };

    /**
     * This function updates the gallery after taking the picture
     */
    private void scanner(String path) {
        Log.i(TAG, "Adding image to gallery via @scanner");
        MediaScannerConnection.scanFile(MainActivity.this,
                new String[] { path }, null,
                new MediaScannerConnection.OnScanCompletedListener() {

                    public void onScanCompleted(String path, Uri uri) {
                        Toast.makeText(MainActivity.this, "Updated gallery", Toast.LENGTH_SHORT).show();
                    }
                });


    }

    /**
     * This function checks if the needed permissions are allowed, if not, it asks for them
     */
    private void askForPermissions(){
        // The request code used in ActivityCompat.requestPermissions()
        // and returned in the Activity's onRequestPermissionsResult()
        Log.i(TAG, "Make sure user has permissions at @askForPermissions");
        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
        }
    }


    /**
     * This function asks for the missing permissions
     * @param context - the context in which we ask
     * @param permissions - the permissions we need
     * @return true if succeeded, else false
     */
    public static boolean hasPermissions(Context context, String... permissions) {
        Log.i(TAG, "Check permissions at @hasPermissions");
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }



}
